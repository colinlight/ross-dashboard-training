//--------------------------
// Build a widget file and merge any extenral scripts contained within the 'scripts' folder
//
// Expected folder structure:
// sample.widget
// scripts 
//		samplewidget.ogscript
//      samplecore.ogscript
// To build the sample widget you would make the following call
// buildWidget( 'sample', 'SampleWidget', [samplecore] );
//
// @Param gridFile : String, name of the parent grid file contaoin view markup
// @Param outputName : String, output file name
// @Param ignoreScripts : Array of Strings, names of script files to ingnore when merging
// @Param customPath : String, if you wish to output to another location on your system add the path here
//--------------------------
function buildWidget( gridFile, outputName, ignoreScripts, customPath ) {

	var fileInput = ogscript.createFileInput(gridFile + ".grid");
	
	var folder = ( customPath == undefined )? "build/" : customPath;
	var outputPath = folder + outputName + ".widgetdescriptor"
	ogscript.saveToFile( outputPath, '<?xml version="1.0" encoding="UTF-8"?>', true );
	var fileOutput = ogscript.createFileOutput ( outputPath, true );
	
	var parse = true;
	var shouldAppend = false;
	do {
		try{
			var str = fileInput.readLine();
			str = str.replace(/\n/, '');

			//find the start 
			if ( !shouldAppend ) {
				if ( str.search("<widgets>" ) != -1  ){
					shouldAppend = true;
					str = "\n\t<widgets>"
				}else{
					continue;
				}
			}

			//find any import scripts and and write them to the widget file
			if ( ( str.search("<api" ) != -1  ) && ( str.search("src=" ) != -1  ) ) {
				var scriptPath = str.substring(str.indexOf('src="') + 5, str.lastIndexOf('.ogscript'));
				var scriptName = scriptPath.substring(scriptPath.indexOf('/') + 1, scriptPath.length);
				var runImmediate =  ( str.search("immediate" ) != -1  )? true : false;
				if ( ( ignoreScripts ) && ( ignoreScripts.indexOf( scriptName ) > -1 ) ){
					//ignore a script
					continue;
				}
				//embed an external script
				appendScript( scriptPath, fileOutput, runImmediate );
				continue;
			}

			//find the end
			if ( str.search("</widgets>" ) != -1  ) {
				parse = false;
			}
			fileOutput.writeLine( str );

		}catch( error ){
			parse = false;
		}
	} while ( parse == true);

	fileInput.close();
	fileOutput.close();

	print( "Build complete!  SOURCE: " +  gridFile + "  OUTPUT: " + outputName);
}


//--------------------------
// Append a script to a file, works in conjunction with the buildWidget function
// @Param path : String input script path
// @Param output : FileOutput object ( File to append to )
// @Param runImmediate : Bool run the script at start
//--------------------------
function appendScript( path, output, runImmediate ){
	var input = ogscript.createFileInput( path + ".ogscript" );
	var startTag = runImmediate == true? '<api immediate="true"><![CDATA[\n' : '<api><![CDATA[\n';
	output.writeString( startTag );
	var parse = true;
	var str;
	do {
		try{
			str = input.readLine();
			str = str.replace(/\n/, '');
			output.writeLine( str );
		}catch( error ){
			parse = false;
		}
	} while ( parse == true);
	//TODO:need to make sure that each input file has an additional line after the final function or code
	//eg.
	//function test(){
	//}
	//<- addtional line here
	input.close();
	output.writeString( "\n]]></api>\n" );
}
