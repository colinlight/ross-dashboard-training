//---------------------------------------------------
//PRIVATE functions for widgets
//---------------------------------------------------
//Import these functions into your widget meta tag e.g.
//<meta>
//    <api src="scripts/widget.ogscript"/>
//</meta>
//---------------------------------------------------


//---------------------------------------------------
//Local print
//---------------------------------------------------
function print(msg) {
    ogscript.debug(msg);
};

//---------------------------------------------------
//Dumb an objects properties
//---------------------------------------------------
function dumpProps(obj) {
    print("START property dump: " + obj);
    for (prop in obj) {
        print("Prop: " + prop);
    }
    print("END property dump..................");
};

//---------------------------------------------------
//Params helper functions
//---------------------------------------------------
//Remove all existing items
//---------------------------------------------------
function paramRemoveAll(paramId) {
    var count = params.getElementCount(paramId);
    for (var i = 0; i <= count; i++) {
        params.getParam(paramId, 0).remove();
    }
};

//---------------------------------------------------
//Widget helper functions
//---------------------------------------------------
//@Param prependStr: String string to add to the start of the ID
function createUUID(prependStr) {
    var uuid = new Date().valueOf() + Math.floor(Math.random() * 1000);
    if (prependStr) {
        return prependStr + uuid;
    }
    return uuid;
};


//@Return the widget base oid
function widgetBaseOID() {
    var self = ogscript.getAttribute("widget");
    return self.getBaseOid();
};


//@Return string the widgets attribute id e.g. set in the id tag -> id="myWidget"
//NOTE: this context is within the private api
function getWidgetElementID() {
    var self = ogscript.getAttribute("widget");
    if (self == null) {
        throw ("Error: you must supply a id for this widget");
    }
    return self.getElementId();
}


//---------------------------------------------------
//Local param event listener caching
//---------------------------------------------------
var listenerObjs = {};
//changedIndex may be null for non-array parameters
function handleChange(param, changedIndex) {
    if (changedIndex != null) {
        print("Status: Changed " + param.getOid() + "[" + changedIndex + "] to " + param.getValueAt(changedIndex));
    } else {
        print("Status: Changed " + param.getOid() + " to " + param.getValue());
    }
};

function attachListener(oid, handler) {
    if (typeof (listenerObjs[oid]) != 'undefined' && listenerObjs[oid] != null) {
        //print("Status: No! Listener already attached to " + oid);
        return;
    }

    var p = params.getParam(oid, 0);
    if (p != null) {
        var changeHandler = (handler == undefined) ? handleChange : handler;
        listenerObjs[oid] = p.addParamListener(changeHandler, true); //function + true/false attach to all elements
    }
};

function destroyListener(oid) {
    if (typeof (listenerObjs[oid]) == 'undefined' || listenerObjs[oid] == null) {
        //print("Status: No listener found for " + oid);
        return;
    }
    listenerObjs[oid].close();
    delete listenerObjs[oid];
};



//---------------------------------------------------
// Notifies listen widgets an event has occured
// @Param: type : String  type of event
// @Param: selection : Object  data to send with the event
// @Param: widgets : Array of widget id's to notify
//---------------------------------------------------
function dispatchDrop(type, selection, widgets) {
    //this will now dispatch/ call the 'drop' function on all listening widgets
    // Get our widget ID.
    var source = getWidgetElementID();
    if (!widgets) return;
    var widgetArray = widgets.split(';');
    for (var i = 0; i < widgetArray.length; i++) {
        var widget = ogscript.getComponentsById(widgetArray[i].trim())[0];
        if (widget != null) {
            widget.call('drop', [source, type, selection]);
        }
    }
};
