# README #


### What is this repository for? ###

* This repository contains all the source code and grid files dashboard training

NOTE: this is the first iteration I will add better notes in the next few days. download the example and code from here:
https://bitbucket.org/colinlight/ross-dashboard-training/downloads/

--------------------------------------

## Useful links

* ### Javascript API https://developer.mozilla.org/en-US/docs/Web/JavaScript


* ### Javascript Online playground https://js.do/


* ### Dashboard Development guide https://rossvideoltd.sharepoint.com/sites/marketing/communications/Shared%20Documents/rossvideo.com/Manuals/Production%20IT%20Systems/DashBoard/DashBoard_CustomPanel_Development_Guide_(8351DR-007).pdf?slrid=41152d9e-e001-4000-98df-e3c736b2975a


* ### Visual Studio code https://code.visualstudio.com/


* ### Setting up Visual Studio code https://stackoverflow.com/questions/29973619/how-to-make-vs-code-to-treat-other-file-extensions-as-certain-language


